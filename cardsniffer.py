from bs4 import *
from HTMLParser import *
import urllib2
import re
import argparse

parser = argparse.ArgumentParser() #yay....a comment
parser.add_argument('-s', type=int, default=1)
parser.add_argument('end', type=int)
parser.add_argument('--verbose', '-v', action='count')
args = parser.parse_args()

# ----------------------------------------

# Exracts the power and toughness of a card, returning them in
# an array of size 2
def getP_T(card_soup):
	p_t_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_ptRow")))
	try:
		p_t = p_t_soup.find(class_="value").text
		p_t = p_t.replace(r' ','').split('/')
		for p, i in enumerate(p_t):
			p_t[p] = int(i)
	except AttributeError:
		p_t = None


	return p_t

# ----------------------------------------

# Gets a single card's info from the source page soup and extracts its info into an array.
def getCardInfo(card_soup):
	info = []
	name_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_nameRow")))
	try:
		name = name_soup.find(class_="value").string
		name = name.strip()
		print("getCardInfo() - Found name: %s"%name);
	except AttributeError:
		name = None
	info.append(name)

	cmc_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_cmcRow")))
	try:
		cmc = cmc_soup.find(class_="value").text
		cmc = int(cmc.strip())
	except AttributeError:
		cmc = None
	info.append(cmc)

	type_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_typeRow")))
	try:
		type_ = type_soup.find(class_="value").string
		type_ = type_.strip()
	except AttributeError:
		type_ = None
	info.append(type_)

	rules_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_textRow")))
	try:
		rules = rules_soup.find(class_="value").text
		rules = rules.strip()
	except AttributeError:
		rules = None
	info.append(rules)

	flavor_soup = BeautifulSoup(str(card_soup.find(id="ctl00_ctl00_ctl00_MainContent_SubContent_SubContent_flavorRow")))
	try:
		flavor = flavor_soup.find(class_="value").text
		flavor = flavor.strip()
	except AttributeError:
		flavor = None
	info.append(flavor)

	info.append(getP_T(card_soup))

	return info

# ----------------------------------------

def processRegularCard(card_soup):
	pass

# ----------------------------------------

# Processes a double-sided card's info into two arrays.
def processDoubleSidedOrHalvedCard(double_card_soup):
	pass

# ----------------------------------------
# main
# ----------------------------------------

start = int(input("Start id: "))
end = int(input("End id: "))

f = open('cards.txt','w')

for id in xrange(start,end + 1):

	page = urllib2.urlopen("http://gatherer.wizards.com/Pages/Card/Details.aspx?multiverseid=" + str(id)).read()
	page = page.decode('utf-8')
	pattern = re.compile(r'[\t\n]+')
	page = re.sub(pattern, '', page)

	card_soup = BeautifulSoup(page)

	cardInfo = getCardInfo(card_soup)

	print("multiverseid=%s\n\tName: %s\n" %(id, cardInfo[0]))
	if verbose >= 1:
		print(/*"multiverseid=%s\n\tName: %s\n"*/"\tCMC:  %s\n\tType: %s\n\tRules Text: %s\n\tFlavor Text: %s" %(/*id, 
			cardInfo[0],*/ cardInfo[1], cardInfo[2], cardInfo[3], cardInfo[4]))
		if len(cardInfo) > 5 and cardInfo[5] is not None:
			print("\tP/T: %i/%i" % (cardInfo[5][0], cardInfo[5][1]))

	f.write("%i, %s, %i, %s, %s, %s" %(id, cardInfo[0], cardInfo[1], cardInfo[2], cardInfo[3], cardInfo[4]))
	if len(cardInfo) > 5 and cardInfo[5] is not None:
		f.write(", %i/%i" % (cardInfo[5][0], cardInfo[5][1]))
	else:
		f.write("None")
	f.write("\n")

f.close
print("All cards written.")